import {login} from "./utils/API";
import {setToken} from "./utils/token";

const loginInput = document.querySelector(".login-input");
const passInput = document.querySelector(".password-input");
const submitBtn = document.querySelector(".submit");

submitBtn.addEventListener("click", () => {
  login(loginInput.value, passInput.value)
  .then(response => response.text())
  .then(token  => {if(token.includes("-")) {
      setToken(token);

      document.location.href = "./index.html"
  } else{alert("Password or login is incorrect");}})
  .catch(() => {
    alert("Password or login is incorrect");
  });
});