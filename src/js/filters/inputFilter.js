const wrapperFilter = document.querySelector(".filter__form");

const changeStr = (el) =>el.textContent.split(":")[1];

wrapperFilter.addEventListener("input", (event) => {
  const colectionCards = document.querySelectorAll(".card-wrapper");
  const cards = [...colectionCards];
  const inputDesc = document.querySelector(".filter__search");
  const inputSelect = document.querySelector(".filter__priority");

  cards.filter((card) => {
    let cardDesc = card.querySelector(".card__description");
    let cardPrior = card.querySelector(".card__priority");

    let textDesc = changeStr(cardDesc)
    let textPrior = changeStr(cardPrior)

    if (textDesc.includes(inputDesc.value)) {
      card.style.opacity = "1";
    } else {
      card.style.opacity = "0";
    }

    if (textPrior.trim() === inputSelect.value || inputSelect.value === "Оберіть пріоритетність") {
      card.style.display = "block";
    } else {
      card.style.display = "none";
    }
  })
})
