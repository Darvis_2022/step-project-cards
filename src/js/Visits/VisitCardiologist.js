import {Visit} from "./Visit";

export class VisitCardiologist extends Visit {
    constructor(id, patientName, doctor, purpose, description, urgency, pressure, bodyMassIndex, pastDiseases, age) {
        super(id, patientName, doctor, purpose, description, urgency);

        this.data = {
            ...this.data,
            pressure,
            bodyMassIndex,
            pastDiseases,
            age
        };
    }

    render(){
        super.render();

        this.pressure = document.createElement('p');
        this.pressure.innerText = `Артеріальний тиск: ${this.data.pressure}`;

        this.bodyMassIndex = document.createElement('p');
        this.bodyMassIndex.innerText = `Індекс маси тіла: ${this.data.bodyMassIndex}`;

        this.pastDiseases = document.createElement('p');
        this.pastDiseases.innerText = `Хвороби: ${this.data.pastDiseases}`;

        this.age = document.createElement('p');
        this.age.innerText = `Вік: ${this.data.age}`;

        this.showMoreInfo.append(this.pressure, this.bodyMassIndex, this.pastDiseases, this.age);

        return this.wrapper;
    }
}
