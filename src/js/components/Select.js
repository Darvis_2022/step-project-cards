export default class Select {
    constructor(options, labelText, isRequired, className, name) {
        this.isRequired = isRequired;
        this.self = document.createElement("div");
        this.label = document.createElement('label');
        this.select = document.createElement("select");

        const defaultOpt = document.createElement("option");
        defaultOpt.textContent = "Зробіть вибір...";
        defaultOpt.disabled = true;
        defaultOpt.selected = true;
        this.select.append(defaultOpt);

        options.forEach(opt => {
            const optionNode = document.createElement("option");
            optionNode.textContent = opt;

            this.select.append(optionNode);
        });

        this.name = name;
        this.labelText = labelText;
        this.className = className;
    };

    set value(itemName){
        for (let option of this.select.options) {
            if(option.text === itemName){
                option.selected = true;
            }
        }
    }

    render() {
        this.label.classList.add('form-label');
        this.label.innerText = this.labelText;
        this.self.append(this.label);

        this.select.classList.add(this.className);
        this.select.setAttribute('name', this.name);
        this.select.required = this.isRequired;
        this.self.append(this.select);

        return this.self;
    };
};